\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{tikz}
\usepackage[linewidth=1pt]{mdframed}
\usepackage{xcolor}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Automatically Generating Behaviour-Driven Development Test Cases From Natural Language Scenario Specifications}

\author{\IEEEauthorblockN{1\textsuperscript{st} Given Name Surname}
\IEEEauthorblockA{\textit{dept. name of organization (of Aff.)} \\
\textit{name of organization (of Aff.)}\\
City, Country \\
email address}
\and
\IEEEauthorblockN{2\textsuperscript{nd} Given Name Surname}
\IEEEauthorblockA{\textit{dept. name of organization (of Aff.)} \\
\textit{name of organization (of Aff.)}\\
City, Country \\
email address}

}

\maketitle

\begin{abstract}
This document is a model and instructions for \LaTeX.
This and the IEEEtran.cls file define the components of your paper [title, text, heads, etc.]. *CRITICAL: Do Not Use Symbols, Special Characters, Footnotes, 
or Math in Paper Title or Abstract.
\end{abstract}

\begin{IEEEkeywords}
component, formatting, style, styling, insert
\end{IEEEkeywords}

\section{Introduction}
Behaviour Driven Development involves clearly outlining the behaviour a system should have and representing that as a scenario that can be tested. Each scenario refers to a bit of code and describes the system behaviour that it will generate, once again helping developers implement only what needs to be implemented. A collection of scenarios serves as living documentation for the software, providing a logical and easy-to-understand representation of how the software should work. These scenarios are then implemented step-by-step to test the expected behaviour of the software. Similarly to TDD, the scenarios and code step implementations are finalized before implementing the system code and they then drive the development process, shaping the design and programming of the system.

 A 2018 survey \cite{survey} exploring the use of BDD and targeting developers from various backgrounds identifies that half of respondents only plan to use BDD in future projects. Respondents also pointed out a number of challenges they come across when using this development technique, including long-term maintenance aspects as well as organisational and collaborative aspects.

Assuming good practice is followed, step implementations are a programmatic translation of the scenario specifications and due to every single step having to be implemented, this process can become costly and time-consuming. Additionally, while the scenario is written in a readable natural language format which is easy to understand without programming knowledge, the step implementations imply the person writing them would have knowledge of the programming language being used by the chosen BDD testing tool.

The Cucumber documentation advises users that developers should be the ones writing the scenarios, as they would also be expected to implement the steps later on. This poses limits on the advantages that natural language specifications can offer in this context. One can derive scenarios from system requirements, but they still need to be a developer to be able to proceed with the next steps in the BDD process. To anyone else, scenarios are no more than living documentation. 

In addition, current BDD technologies only ease the work of a developer to a certain extent. If we take, for example, Behave\cite{behave}, the expected BDD workflow would be:

\begin{enumerate}
  \item Write features (scenarios) based on requirements.
  \item Implement code steps (given, when, then steps described in the scenario).
  \item Write the implementation needed for the test to pass.
\end{enumerate}

\begin{figure}[htbp]
\centering

\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt        

\begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
%uncomment if require: \path (0,445); %set diagram left start at 0, and has height of 445

%Rounded Rect [id:dp11615707118553653] 
\draw   (376,153.6) .. controls (376,148.3) and (380.3,144) .. (385.6,144) -- (440.9,144) .. controls (446.2,144) and (450.5,148.3) .. (450.5,153.6) -- (450.5,182.4) .. controls (450.5,187.7) and (446.2,192) .. (440.9,192) -- (385.6,192) .. controls (380.3,192) and (376,187.7) .. (376,182.4) -- cycle ;
%Rounded Rect [id:dp28319078972834066] 
\draw   (96.5,157) .. controls (96.5,152.58) and (100.08,149) .. (104.5,149) -- (160.5,149) .. controls (164.92,149) and (168.5,152.58) .. (168.5,157) -- (168.5,181) .. controls (168.5,185.42) and (164.92,189) .. (160.5,189) -- (104.5,189) .. controls (100.08,189) and (96.5,185.42) .. (96.5,181) -- cycle ;
%Straight Lines [id:da3902283118372285] 
\draw    (169.5,169) -- (189.5,169) ;
\draw [shift={(191.5,169)}, rotate = 180] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

%Straight Lines [id:da2315590544710866] 
\draw    (353.5,167) -- (374.5,167) ;
\draw [shift={(376.5,167)}, rotate = 180] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

%Rounded Rect [id:dp4890348215681035] 
\draw   (192.5,154.8) .. controls (192.5,149.94) and (196.44,146) .. (201.3,146) -- (249.7,146) .. controls (254.56,146) and (258.5,149.94) .. (258.5,154.8) -- (258.5,181.2) .. controls (258.5,186.06) and (254.56,190) .. (249.7,190) -- (201.3,190) .. controls (196.44,190) and (192.5,186.06) .. (192.5,181.2) -- cycle ;
%Rounded Rect [id:dp7925396602047801] 
\draw   (281.5,153.8) .. controls (281.5,148.94) and (285.44,145) .. (290.3,145) -- (343.7,145) .. controls (348.56,145) and (352.5,148.94) .. (352.5,153.8) -- (352.5,180.2) .. controls (352.5,185.06) and (348.56,189) .. (343.7,189) -- (290.3,189) .. controls (285.44,189) and (281.5,185.06) .. (281.5,180.2) -- cycle ;
%Straight Lines [id:da5175559845310924] 
\draw    (258.5,168) -- (278.5,168) ;
\draw [shift={(280.5,168)}, rotate = 180] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;


% Text Node
\draw (414.25,167) node [scale=0.7] [align=left] {Software \\Implementation};
% Text Node
\draw (132,167.5) node [scale=0.7] [align=left] {Requirements};
% Text Node
\draw (226,166.5) node [scale=0.7] [align=left] {Scenarios};
% Text Node
\draw (317,167) node [scale=0.7] [align=left] {Code Steps};


\end{tikzpicture}
\caption{Standard BDD Workflow}
\label{fig}
\end{figure}
As modern software development often involves incremental design and continuous communication with the clients over the course of the development process, it is not unusual that changes are made to the initial requirements, whether it is because new features are requested, or it is based on evaluation feedback. Given the current BDD workflow outlined above, the development team would need to adjust to these modifications by rewriting scenarios as well as their implementations. Depending on the complexity of a software system, this process can become time-costly and lead to inconsistencies which could affect the development process as a whole.

\begin{figure}[htbp]


%Rounded Rect [i\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt        

\begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
%uncomment d:dp3518822006900416] 
\draw   (192,147.3) .. controls (192,136.64) and (200.64,128) .. (211.3,128) -- (269.2,128) .. controls (279.86,128) and (288.5,136.64) .. (288.5,147.3) -- (288.5,209.7) .. controls (288.5,220.36) and (279.86,229) .. (269.2,229) -- (211.3,229) .. controls (200.64,229) and (192,220.36) .. (192,209.7) -- cycle ;
%Rounded Rect [id:dp11615707118553653] 
\draw   (321,155.2) .. controls (321,149.57) and (325.57,145) .. (331.2,145) -- (394.3,145) .. controls (399.93,145) and (404.5,149.57) .. (404.5,155.2) -- (404.5,185.8) .. controls (404.5,191.43) and (399.93,196) .. (394.3,196) -- (331.2,196) .. controls (325.57,196) and (321,191.43) .. (321,185.8) -- cycle ;
%Rounded Rect [id:dp28319078972834066] 
\draw   (87.5,154.6) .. controls (87.5,149.85) and (91.35,146) .. (96.1,146) -- (159.9,146) .. controls (164.65,146) and (168.5,149.85) .. (168.5,154.6) -- (168.5,180.4) .. controls (168.5,185.15) and (164.65,189) .. (159.9,189) -- (96.1,189) .. controls (91.35,189) and (87.5,185.15) .. (87.5,180.4) -- cycle ;
%Straight Lines [id:da7665355774182268] 
\draw    (240.25,152.5) -- (240.25,176.5) ;
\draw [shift={(240.25,178.5)}, rotate = 270] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

%Straight Lines [id:da3902283118372285] 
\draw    (169.5,169) -- (189.5,169) ;
\draw [shift={(191.5,169)}, rotate = 180] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

%Straight Lines [id:da2315590544710866] 
\draw    (287.5,172) -- (317.5,172) ;
\draw [shift={(319.5,172)}, rotate = 180] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;


% Text Node
\draw (242,145) node [scale=0.7] [align=left] {Scenarios};
% Text Node
\draw (364.75,169.5) node [scale=0.7] [align=left] {Software \\Implementation};
% Text Node
\draw (128,167.5) node [scale=0.7] [align=left] {Requirements};
% Text Node
\draw (246,201) node [scale=0.7] [align=left] {auomatically \\generated \\code steps};


\end{tikzpicture}
\caption{Desired BDD Workflow}
\label{fig}
\end{figure}

In order to explore solutions to these challenges, we ask the following research questions:

\begin{itemize}
  \item \textbf{RQ1:} Can we predict if a code step is a Given, When or Then step?
  \item \textbf{RQ2:} Can we map a code step to the scenario it implements?
  \item \textbf{RQ3:} Can we generate code steps from natural language scenarios?
\end{itemize}

+++++ MORE ABOUT THE PROPOSED SOLUTION, TOOL, RESULTS, NOVEL CONTRIBUTIONS

\section{Related Work}

\subsection{Behaviour-Driven Development}
In its early stages, BDD was viewed as a solution to the limitations of TDD. North \cite{north} introduced BDD in 2006 as a design approach that would help developers encapsulate agile techniques into their development process. He proposed referring to tests as behaviours instead, which he believed would help a development team know exactly what their software should do and focus the implementation on just what is needed, reducing the amount of redundant code and unnecessary functionality. 

In its early stages, BDD was misused as a way to better meet and understand requirements and to reduce the difference between initial expectations and the functionality provided by the final product\cite{tav}\cite{Carvalho2010FillingTG}\cite{Carrera2014}. As it became more widespread, the software industry reached a better understanding of what the design approach can offer.
Keogh \cite{keogh} recognizes the wide range of the advantages that BDD can bring to the software development process and describes it as an approach to delivering software that matters emphasizing the fact that it helps developers deliver no more than what is necessary as well as identify areas where more learning is needed. 
\subsection{NLP and Requirements Engineering}

Recent advancements and research suggest that requirements engineers should take advantage of current NLP technologies not only to ease their work but also to help continue to study and improve the flow from natural to formal language. With recent improvements in NLP techniques and a growth in interest from industry, Ferrari \cite{8449650} outlined the issues that could arise from applying these techniques in industry, such as a need for large datasets for the NLP algorithms to work properly, a need for individuals with domain experience to take part in the process which might be a problem due to confidentiality and the fact that the context differs among companies so a functioning general solution might not exist. 

Ferrari et. al \cite{8106888} define the concept of Natural Language Requirements Processing as a four-dimensional framework involving discipline, dynamism, domain knowledge and datasets. They predict the appearance of a strict writing standard when it comes to requirements, that requirements engineering tools will start implementing NLP techniques, that the use of industry communication channels (e.g. Slack) will make domain knowledge more accessible and that the collaboration between research and industry will help facilitate more datasets. 

\subsection{Test Case Generation}

There has been extensive research into automatically generating test cases and there are two general approaches: generating diagrams from system requirements and using those to generate test cases \cite{809349} \cite{ismail}, or representing the system requirements using a formal specification language \cite{Wang:2015:AGS:2771783.2771812}. The first approach does not eliminate an intermediate step between the system specification and the test case and the second approach generally uses heavily structured formal specifications that lack the natural language aspect, thus increasing the amount of work needed to translate the initial requirements.

In closely related previous work, Kamalakar \cite{sunil} identifies the importance of eliminating this additional step in the BDD process and develops a tool that automatically generates test cases, but it is limited to projects that have already been implemented, which contradicts the core BDD aspect of writing tests before implementing the system code.

\section{Tool}

We developed a tool that can be imported into a Python project as a Python package, which accesses the features folder of in the project path and uses the scenarios it finds to generate a new file containing code step implementations in the correct path.

The tool comprises of three main components:
\begin{itemize}
    \item \textit{Parsing component}: extracts the relevant information from the scenario specifications using NLP techniques such as part-of-speech tagging and shallow semantic parsing.
    \item \textit{Generating component}: generates code step implementations using the information from the parser and stores them into a steps.py file.
    \item \textit{Change Detector}: detects changes made to the project API or the specifications and notifies that the code step implementations need to be generated again
\end{itemize}

+++++ SYSTEM DIAGRAM
\section{Method}
\subsection{Classifying Experiment}

A common BDD scenario will provide a description of a Given, a When and a Then step. Each of these scenario steps in turn will have a corresponding implementation, meant to set up the scenario context (Given), perform an action (When) and verify the outcomes of that action (Then). 

This led to the question of whether the implementations of each of these types of steps follow a certain pattern. Due to scenario implementations being divided into implementations of self-contained steps with very specific roles, we identified the following common features among step categories :
\begin{itemize}
    \item \textit{Given steps} - setting up the scenario context:
        \begin{itemize}
            \item constructor call - instantiating a new object to be used in the execution of the scenario.
            \item assignment to the context variable - bringing the new object into the scenario context.
            \end{itemize}
        
    \item \textit{When steps} - performing an action:
        \begin{itemize}
            \item method call on context variable - making an event take place in the given context.
        \end{itemize}
    \item \textit{Then steps} - verifying the consequences of that action to see if the software is behaving appropriately:
        \begin{itemize}
            \item assertions - checking if the outcome of the even triggered in the When step is the one expected in this scenario.
            \end{itemize}
    \end{itemize}

We accessed the source code of step implementation functions in the steps folder, as well as information about the functions themselves, such as function arguments and class definitions of objects used in the function. Once this information was available, we extracted the features outlined above and built a training set containing all the individual step implementation labeled as Given, When or Then. The resulting set was be used to train a naive Bayes classifier, which then determined the accuracy of classifying a test set of unseen step implementations.

The experiment was run on a sample of open-sourced GitHub projects written in Python that use BDD. The sample comprised of 985 code step implementations, 100 of which were used as a training set and the rest as a test set.
\subsection{Matching Experiment}

Python Behave identifies step implementations using a combination of annotations and function decorators:
\begin{itemize}
    \item annotations can be either "@given", "@when" or "@then"
    \item decorators are strings that usually contain the rest of the scenario step description.
\end{itemize}

After exploring whether we can determine if a certain code step implements a Given, When or Then scenario step, it would be useful to see whether we can match a step implementation with the exact scenario step it implements. Similar to the above mentioned experiment, this would ultimately help determine what a natural language scenario specification can tell us about the step implementation code.

Using the same sample as the one in the first experiment, we calculated string similarity between scenario step descriptions found in project feature files (e.g. "Given a bank account with initial balance of 1000", "When we withdraw 100 dollars from the account", "Then the balance of the account should be 900") and a step implementation annotation descriptor - following good practice, the information in the step implementation descriptor should be similar to the scenario step description, if not identical. The strings are converted to a word vector format which provides multi-dimensional meaning representations of each word in the string. The proximity of these word vectors in the vector space determines how similar they are. The similarity of sequences of words (e.g. sentences, phrases) are calculated by averaging the similarity scores of all the words in the given sequences.

We compared each individual scenario step description against all the available step annotations and selected the one with the highest similarity score. A successful match occurred when the highest similarity score pointed towards the correct corresponding descriptor of the scenario step it implements.

\subsection{Test Case Generation Tool}

The previously described experiments offered a clearer idea of how to translate from natural language scenario descriptions to code step implementations by generating the necessary code from the scenario specification and added onto our initial assumptions. 

In order to evaluate the performance of our tool and the feasibility of test case generation using only scenario specifications, we used a combination of mutation testing and API comparison. We ran the tool on a sample of open-sourced GitHub projects that use BDD and generated new step implementations of the scenarios described in the existing feature files. We then ran a mutation testing tool on the projects and checked whether the same effect occurred when using the newly generated step files as when using the original ones. Additionally, we compared the project structure generated using just entities identified in the specifications (class, function and constructor names, class attributes,function arguments) with the ones in the existent project API.


\section{Results}
\subsection{Classifying Experiment}
\subsection{Matching Experiment}
\subsection{Test Case Generation Tool}
\section{Discussion}
\section{Conclusion}


\section*{Acknowledgment}

The preferred spelling of the word ``acknowledgment'' in America is without 
an ``e'' after the ``g''. Avoid the stilted expression ``one of us (R. B. 
G.) thanks $\ldots$''. Instead, try ``R. B. G. thanks$\ldots$''. Put sponsor 
acknowledgments in the unnumbered footnote on the first page.

\bibliography{bibliography}
\bibliographystyle{plain}
\end{document}
